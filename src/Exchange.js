import React from 'react'


  

  class Exchange extends React.Component{
    constructor(props){
        super(props);
            this.duit = ["USD","EUR","IDR", "SGD"]
            this.cached={}
            this.state = {
                base: "USD",
                target:"IDR",
                awalValue: 0,
                akhirValue: 0
            }
        
    }
    componentDidMount(){
        const awalValue = localStorage.getItem("awalValue");
        const akhirValue = localStorage.getItem("akhirValue");
        
        if (awalValue!== null && akhirValue!== null){
            this.setState({
            awalValue: parseInt(awalValue),
            akhirValue: parseInt(akhirValue)
        })

        }
        window.addEventListener(`beforeunload`, () => {
            localStorage.setItem("awalValue",this.state.awalValue)
            localStorage.setItem("akhirValue",this.state.akhirValue)
        }) 
        
        
    }
    
    render(){
        return (
            <div>
            <div>
            <select onChange={this.pilihPilihan} name="base" value={this.state.base}>
            {this.duit.map(currency => <option key={currency} value={currency}>{currency}</option>)}
            </select>
            <input onChange = {this.inputanAngka} value={this.state.awalValue}/>
            </div>
            
            <div>
            <select onChange={this.pilihPilihan} name="target" value={this.state.target}>
            {this.duit.map(currency => <option key={currency} value={currency}>{currency}</option>)}
            </select>
            <input disabled={true} value={this.state.akhirValue === null ? "Calculating . ," : this.state.akhirValue}/>
            </div>
            
            </div>              
            
        )
    }
    pilihPilihan = (event)=> {
        this.setState({
            [event.target.name]:event.target.value,
            akhirValue:null
        }, this.langsungHitung)
    }
    inputanAngka = (event)=>{
        this.setState({
            awalValue:event.target.value
        }, this.langsungHitung)

    }

    langsungHitung = ()=> {
        const baruValue = parseFloat(this.state.awalValue);
        if(isNaN(baruValue)){
            return;
        }

        if(this.cached[this.state.awalValue]!== undefined && Date.now() - this.cached[this.state.awalValue].timestamp < (1000*60)){
            this.setState({
                akhirValue: this.cached[this.state.awalValue].rates[this.state.target]* baruValue
            })
        }
        fetch(`https://api.exchangeratesapi.io/latest?base=${this.state.base}`)//latest kemudian ?base= untuk mengganti nilai base default web menjadi base bawaan kita
        .then(response => response.json())
        .then(data => {
                this.cached[this.state.base]={
                    rates: data.rates,
                    timestamp: Date.now()
                }
                this.setState({akhirValue : data.rates[this.state.target] * baruValue
                })
        })
    }
    
    
    
}
  export default Exchange;
  
